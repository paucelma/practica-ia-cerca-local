import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import numpy as np
import os

index = np.arange(100,550,50 )

#----------------------------------------

arrX1 = np.array([[176.66, 145.99, 142.85], [175.19, 146.97, 144.81], [186.61, 146.96, 142.42], [179.73, 144.51, 142.81]])
count = 0
x = np.array(range(0,3), float)
y = np.array(range(0,4), float)
xpos, ypos = np.meshgrid(x, y)
xpos = xpos.flatten()
ypos = ypos.flatten()
colors = ["b", "g", "crimson"]*4
zpos = np.zeros_like(xpos)
dx = 0.5*np.ones_like(zpos)
dy = 0.75*np.ones_like(zpos)
dz = arrX1.flatten()
fig = plt.figure(figsize =(10, 7))
ax = fig.add_subplot(111, projection = "3d")
ax.set_title("Heuristica en funcion de K y Lambda", fontsize = 15, linespacing=40)
ax.set_xticks(range(3))
ax.set_xticklabels(["L = 1.0", "L = 0.1", "L = 0.01"])
ax.set_xlabel("Lambda values", labelpad = 10)
ax.set_yticks(range(4))
ax.set_yticklabels(["K = 1", "K = 5", "K = 25", "K = 125"])
ax.set_ylabel("K values", labelpad = 10)
ax.set_zticklabels([ "522.5", "545", "567.5", "590", "612.5", "635", "657.5", "680"])
ax.set_zlabel("Heuristica", labelpad = 10)
ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color=colors)

plt.show()
