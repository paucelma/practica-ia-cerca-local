package IA.Azamon;

//~--- non-JDK imports --------------------------------------------------------


import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;



/**
 *
 */
public class AzamonSuccessorFunction implements SuccessorFunction {
    @SuppressWarnings("unchecked")
    public List getSuccessors(Object aState) {
        ArrayList               retVal = new ArrayList();
        AzamonBoard             board  = (AzamonBoard) aState;
        AzamonHeuristicFunction AzamonHF  = new AzamonHeuristicFunction();
        int np = board.getNPackages();
        int no = board.getNTransport();
        String S = "";


        //nodos generados por el swap
        for (int i = 0; i < np; i++) {
            for (int j = i + 1; j < np; j++) {
                AzamonBoard newBoard = new AzamonBoard(board.getPackages(), board.getTransport(),
                                           board.getAssignment(), board.getFreespace(), board.getPrice(), board.getPriceHappiness(), board.gethappyConst());
                if(newBoard.swapCompatible(i,j) ){
                    newBoard.swapPackages(i,j);
                    // System.out.printf("Heuristic %f ----- %f\n", newBoard.getPrice(), newBoard.getPriceHappiness());
                     // System.out.printf("Heuristic 2----%f \n",newBoard.getPriceHappiness());
                    double v = AzamonHF.getHeuristicValue(newBoard);
                    S = " " + i + " " + j + " swap(" + v + ")";
                    retVal.add(new Successor(S , newBoard));
                }
            }
           //nodos generados por el move
            for(int j = 0; j < no; j++){
                AzamonBoard newBoard = new AzamonBoard(board.getPackages(), board.getTransport(),
                                           board.getAssignment(), board.getFreespace(), board.getPrice(), board.getPriceHappiness(), board.gethappyConst());
                if(newBoard.compatible(i,j) ){
                    newBoard.movePackage(i,j);
                    double v = AzamonHF.getHeuristicValue(newBoard);
                    S = " " + i + " " + j + " move(" + v + ")";
                    retVal.add(new Successor(S , newBoard));
                    // System.out.printf("Heuristic second for %f\n",newBoard.getPriceHappiness());
                }
            }
        }
        // System.out.printf("Heuristic %f ----- %f\n", board.getPrice(), board.getPriceHappiness());
        // System.out.printf("Heuristic is %f\n",board.getPriceHappiness());
        return retVal;
    }
}
