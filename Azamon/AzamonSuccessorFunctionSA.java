package IA.Azamon;

//~--- non-JDK imports --------------------------------------------------------


import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;
import aima.util.Pair;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Javier Bejar
 *
 */
public class AzamonSuccessorFunctionSA implements SuccessorFunction {
    @SuppressWarnings("unchecked")
    public List getSuccessors(Object aState) {
        ArrayList retVal = new ArrayList();
        AzamonBoard board  = (AzamonBoard) aState;
        AzamonHeuristicFunction azamonHeuristicFunction  = new AzamonHeuristicFunction();
        Random myRandom=new Random();
        int i,j;
        int npacks = board.getNPackages();
        int ntransp = board.getNTransport();
        String S = "";


        AzamonBoard newBoard = new AzamonBoard(board.getPackages(), board.getTransport(),
                    board.getAssignment(), board.getFreespace(), board.getPrice(), board.getPriceHappiness(), board.gethappyConst());

        List<Pair> operadoresValidos;

        do {
            operadoresValidos = new ArrayList<>();
            i = myRandom.nextInt(board.getNPackages());
            for (j = 0; j < ntransp; ++j) {
                if (board.compatible(i, j) && board.getAssignmentOfPackage(i) != j) {
                    operadoresValidos.add(new Pair("move", j));
                }
            }
            for (j = 0; j < npacks; ++j) {
                if (board.getAssignmentOfPackage(i) != board.getAssignmentOfPackage(j)
                        && board.swapCompatible(i, j)) {
                    operadoresValidos.add(new Pair("swap", j));
                }
            }
        } while(operadoresValidos.isEmpty());

        int ind = myRandom.nextInt(operadoresValidos.size());
        Pair op = operadoresValidos.get(ind);

        if(op.getFirst().equals("move")) {
            S = "MOVE Package " + i + " to Offer " + (Integer)op.getSecond();
            newBoard.movePackage(i,(Integer)op.getSecond());
        } else if(op.getFirst().equals("swap")) {
            S = "SWAP Package " + i + " with Package " + (Integer)op.getSecond();
            newBoard.swapPackages(i, (Integer)op.getSecond());
        }

        double v = azamonHeuristicFunction.getHeuristicValue(newBoard);

        retVal.add(new Successor(S, newBoard));

        return retVal;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
