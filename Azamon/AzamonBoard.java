package IA.Azamon;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collections;
import java.util.Comparator;

public class AzamonBoard {

    /// Numero de paquetes
    private int npackages;

    /// Numero de ofertas
    private int noffers;

    /// Lista con los paquetes del enunciado
    private Paquetes packages;

    /// Lista con las ofertas del enunciado
    private Transporte transport;

    /// Asignación de paquetes a ofertas
    private int[] assignment;

    /// Espacio libre en las ofertas
    private double[] freespace;

    /// Coste heuristica
    private double price;

    /// Coste heuristica felicidad
    private double happinessPrice;

    /// Constante para calcular la heuristica felicidad
    private double happyConst;
    /*!\brief Genera una instancia del problema de Azamon
     *
     * @param [in] nc Numero de ciudades
     */
    public AzamonBoard(Paquetes p, Transporte t, int ini) {
        npackages = p.size();
        noffers = t.size();
        packages = p;
        transport = t;
        assignment = new int[npackages];
        freespace = new double[noffers];
        price = 0.0;
        happinessPrice = 0.0;
        happyConst = 5;

       for(int i = 0; i < noffers; ++i) freespace[i] = transport.get(i).getPesomax();
       for(int j = 0; j < npackages; ++j) {
         assignment[j] = -1;
       }

        Collections.sort(packages, new PaqueteComparatorPrioridad());

        if (ini==1){
            Collections.sort(packages, new PaqueteComparatorPeso());
            Collections.sort(transport, new OfertaComparator());
            for(int i = 0; i < noffers; ++i) freespace[i] = transport.get(i).getPesomax();
        }
        if (!backtrackingAssignment(assignment.clone(), freespace.clone(), 0))
            System.out.println("Solucion no encontrada");
        initialPriceHappiness();
        initialPrice();
        //TODO no se si deberia ponerlo dentro de los if para evitar el caso en que no encontramos solucion
    }


    /*!\brief Genera una instancia del problema de Azamon con la constante happyCost = Happy
     *
     * @param [in] nc Numero de ciudades
     */
    public AzamonBoard(Paquetes p, Transporte t, int ini, double Happy) {
        npackages = p.size();
        noffers = t.size();
        packages = p;
        transport = t;
        assignment = new int[npackages];
        freespace = new double[noffers];
        price = 0.0;
        happinessPrice = 0.0;
        happyConst = Happy;

       for(int i = 0; i < noffers; ++i) freespace[i] = transport.get(i).getPesomax();
       for(int j = 0; j < npackages; ++j) {
         assignment[j] = -1;
       }

        Collections.sort(packages, new PaqueteComparatorPrioridad());

        if (ini==1){
            Collections.sort(packages, new PaqueteComparatorPeso());
            Collections.sort(transport, new OfertaComparator());
            for(int i = 0; i < noffers; ++i) freespace[i] = transport.get(i).getPesomax();
        }
        if (!backtrackingAssignment(assignment.clone(), freespace.clone(), 0))
            System.out.println("Solucion no encontrada");
       initialPriceHappiness();
       initialPrice();
       //TODO no se si deberia ponerlo dentro de los if para evitar el caso en que no encontramos solucion
    }

    public AzamonBoard(Paquetes p, Transporte t, int[] assign, double[] space, double c, double cH, double Happy) {
        npackages = p.size();
        noffers = t.size();
        packages = p;
        transport = t;
        assignment = assign.clone();
        freespace = space.clone();
        price = c;
        happinessPrice = cH;
        happyConst = Happy;
    }

    public AzamonBoard(Paquetes p, Transporte t, int[] assign, double[] space, double c) {
        npackages = p.size();
        noffers = t.size();
        packages = p;
        transport = t;
        assignment = assign.clone();
        freespace = space.clone();
        price = c;
        happinessPrice = c;
        happyConst = 5;
    }

    /*!\brief Hace una asignación inicial de paquetes a ofertas.
     *
     */
    public boolean backtrackingAssignment(int[] assign, double[] fs, int i) {

        /* Caso base: Si todos los paquetes estan asignados, return true */
        if(i >= npackages){
            assignment = assign;
            freespace = fs;
            return true;
        }

        /* Consideramos un paquete y lo intentamos poner en todas las ofertas */
        Paquete p = packages.get(i);

        /* Si encontramos una oferta compatible, llamamos a la función
        recursiva e intentamos poner el siguiente paquete. Si esta combinacion no funciona,
        seguimos iterando,
         */

        /* Partimos la asignación en 3 para dar más peso a los paquetes según prioridad */

        for(int j = 0; j < noffers; ++j) {
            if(compatible(i,j, fs[j])) {
                assign[i] = j;
                fs[j] -= p.getPeso();
                if(backtrackingAssignment(assign.clone(), fs.clone(), i+1)) return true;
                else {
                    assign[i] = -1;
                    fs[j] += p.getPeso();
                }
            }
        }

        /* Si no encontramos solución por este camino, devolvemos un false y tiramos para atrás */
        return false;
    }

    /*!\brief Calcula el precio del estado inicial
     *
     */
    private void initialPrice() {
        double costPackOff,costStorage,sum=0;
        double pesoPack;
        for(int i=0;i<npackages;i++){
            pesoPack = packages.get(i).getPeso();
            costPackOff = transport.get(assignment[i]).getPrecio()*pesoPack;
            costStorage = getDiasAlmacen(assignment[i])*0.25*pesoPack;
            sum = sum + costPackOff + costStorage;
        }
        price = sum;
    }

    private void initialPriceHappiness() {
        double costPackOff,costStorage,sum=0;
        double pesoPack;
        for(int i=0;i<npackages;i++){
            pesoPack = packages.get(i).getPeso();
            costPackOff = transport.get(assignment[i]).getPrecio()*pesoPack;
            costStorage = getDiasAlmacen(assignment[i])*0.25*pesoPack;
            sum = sum + costPackOff + costStorage - (getDiasAnte(i, assignment[i])*happyConst);
        }
        happinessPrice = sum;
    }

    public boolean swapCompatible(int paq1, int paq2) {
        double space1 = freespace[assignment[paq1]];
        space1 += packages.get(paq1).getPeso();
        double space2 = freespace[assignment[paq2]];
        space2 += packages.get(paq2).getPeso();

        return compatible(paq1, assignment[paq2], space2) &&
                compatible(paq2, assignment[paq1], space1);
    }

    public boolean compatible(int i, int j) {
        return compatible(i, j, freespace[j]);
    }

    /*!\brief Indica si un paquete es compatible con una oferta.
     *
     */
    public boolean compatible(int i, int j, double peso) {
        Oferta o = transport.get(j);
        Paquete p = packages.get(i);
        boolean diasCompatible = o.getDias() <= getDiasFromPrioridad(p.getPrioridad());
        boolean pesoCompatible = peso >= p.getPeso();
        return diasCompatible && pesoCompatible;
    }

    public int getDiasFromPrioridad(int p) {
        return 2*p+1;
    }

    /*!\brief Retorna el numero de paquetes de la instancia
     *
     */
    public int getNPackages() {return(npackages);}

    /*!\brief Retorna el numero de dias que un paquete pasa en el almacen
     *
     */
    public int getDiasAlmacen(int i) {
      int diasA;
      switch(transport.get(i).getDias()){
        case 3:case 4: diasA = 1; break;
        case 5: diasA =  2; break;
        default: diasA =  0; break;
      }
      return diasA;
    }

    /*!\brief Retorna el numero de ofertas de la instancia
     *
     */
    public int getNTransport() {return(noffers);}

    /*!\brief Retorna los paquetes
     *
     */
    public Paquetes getPackages() {return(packages);}

    public int getAssignmentOfPackage(int p) {return assignment[p];}

    /*!\brief Retorna las ofertas
     *
     */
    public Transporte getTransport() {return(transport);}

    /*!\brief Retorna la asignacion de paquetes a ofertas
     *
     */
    public int[] getAssignment() {return(assignment);}

    /*!\brief Retorna el espacio libre en las ofertas
     *
     */
    public double[] getFreespace() {return freespace;}

    /*!\brief Retorna el precio de la oferta j
     *
     */
    public double getOfferPrice(int j) {return(transport.get(j).getPrecio());}

    /*!\brief Retorna la constante para la heuristica Happy
     *
     */
    public double gethappyConst() {return happyConst;}

    /*!\brief Retorna el peso del paquete i
     *
     */
    public double getPeso(int i) {return(packages.get(i).getPeso());}

    /*!\brief Retorna los dias de antelación del paquete i en la oferta j
     *
     */
    public double getDiasAnte(int i, int j) {
      int diasPrio = getDiasFromPrioridad(packages.get(i).getPrioridad()) - 1;
      int diasOferta = getDayOffer(j);
      if (diasPrio == 0 || diasPrio <= diasOferta) return 0;
      else return(diasPrio - diasOferta);
    }

    /*!\brief Retorna el dia de salida de entrega de la oferta j
     *
     */
    public int getDayOffer(int j) {return(transport.get(j).getDias());}

    /*!\brief Retorna el coste de la prioridad del paquete i
     *
     */
    public double getPriorityPrice(int i) {
        int priority = packages.get(i).getPrioridad();
        double cost;
        switch (priority) {
            case 0: cost = 5; break;
            case 1: cost = 3; break;
            default: cost = 1.5; break;
        }
        return(cost);
    }

    /*!\brief Retorna el coste de la solucion acutal
     *
     */
    public double getPrice() {
        return(price);
    }

    /*!\brief Retorna el coste teniendo en cuenta la felicidad de la solucion acutal
     *
     */
    public double getPriceHappiness() {
        return(happinessPrice);
    }


    /*!\Brief Designa el coste de la solucion acutal en base al cambio del paquete i de la oferta j
     *
     * \pre los valores han de ser validos
     */
    public void setPrice(int i, int j, int oldOffer){
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("#.##########",dfs);

        double packWeight = packages.get(i).getPeso();
        double priceStorageOld = 0;
        double priceOfferKgOld = 0;
        double priceOld = 0;
        if(oldOffer != -1){
           // resto el precio de antes
           priceStorageOld = 0.25*packWeight*getDiasAlmacen(oldOffer);
           priceOfferKgOld = packWeight*transport.get(oldOffer).getPrecio();
           priceOld =  priceOfferKgOld + priceStorageOld;
        }
        // sumo el nuevo coste
        double priceStorage = 0.25*packWeight*getDiasAlmacen(j);
        double priceOfferKg = packWeight*transport.get(j).getPrecio();
        //System.out.printf("Precio Hasta ahora: %f  ->>> Nuevo: ",price);
        price = price + priceStorage + priceOfferKg -priceOld;
        String s = df.format(price);
        price = Double.parseDouble(s);
        //System.out.printf("suma %f = %f+%f +( %f) \n ",price,priceStorage,priceOfferKg, priceOld);
    }

    /*!\Brief Designa el coste, teniendo en cuenta la felicidad, de la solucion acutal en base al cambio del paquete i de la oferta j
     *
     * \pre los valores han de ser validos
     */
    public void setPriceHappiness(int i, int j, int oldOffer){
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("#.##########",dfs);

        double packWeight = packages.get(i).getPeso();
        double priceStorageOld = 0;
        double priceOfferKgOld = 0;
        double priceOld = 0;
        if(oldOffer != -1){
           priceStorageOld = 0.25*packWeight*getDiasAlmacen(oldOffer);
           priceOfferKgOld = packWeight*transport.get(oldOffer).getPrecio();
           priceOld =  priceOfferKgOld + priceStorageOld - (getDiasAnte(i, oldOffer)*happyConst);
        }
        // sumo el nuevo coste
        double priceStorage = 0.25*packWeight*getDiasAlmacen(j);
        double priceOfferKg = packWeight*transport.get(j).getPrecio();
        //System.out.printf("Precio Hasta ahora: %f  ->>> Nuevo: ",price);
        happinessPrice = happinessPrice + priceStorage + priceOfferKg - (getDiasAnte(i, j)*happyConst) - priceOld;
        happinessPrice = Double.parseDouble(df.format(happinessPrice));
        //System.out.printf("suma %f = %f+%f +( %f) \n ",price,priceStorage,priceOfferKg, priceOld);
    }

    /*!\brief Enseña por pantalla la solucion actual
     *
     */
    public void showState() {
        for(int i = 0; i < npackages; ++i) {
            System.out.println("Paquete " + i + ": Oferta " + assignment[i]);
        }
        System.out.println();
        for(int j = 0; j < noffers; ++j) {
            System.out.println("Peso restante en oferta " + j + ": " + freespace[j]);
        }
    }
    /*!\Brief Intercambio de dos paquetes a la oferta del otro
     *
     * \pre los valores han de ser validos
     */
    public void swapPackages(int i, int j){
        int  Ioffer, Joffer;
        Ioffer = assignment[i];
        Joffer = assignment[j];
        setPrice(i, Joffer,Ioffer);
        setPrice(j,Ioffer,Joffer);
        setPriceHappiness(i, Joffer,Ioffer);
        setPriceHappiness(j,Ioffer,Joffer);
        assignment[i] = Joffer;
        assignment[j] = Ioffer;
        freespace[Ioffer] = freespace[Ioffer] +packages.get(i).getPeso()-packages.get(j).getPeso();
        freespace[Joffer] = freespace[Joffer] +packages.get(j).getPeso()-packages.get(i).getPeso();
    }

    /*!\Brief Mueve un paquete i a una oferta j
     *
     * \pre los valores han de ser validos
     */
    public void movePackage(int i, int j){
        int oldOffer = assignment[i];
        setPrice(i,j,oldOffer);
        setPriceHappiness(i,j,oldOffer);
        assignment[i] = j;
        double packWeight = packages.get(i).getPeso();
        if (oldOffer != -1) freespace[oldOffer] = freespace[oldOffer] +packWeight;
        freespace[j] = freespace[j] - packWeight;
    }
    /*!\Brief Intercambio de dos paquetes a la oferta del otro devuelve false en caso de fallo
     *
     * \pre los valores han de ser validos
     */
    public boolean swapPackages_p(int i, int j){
        int  Ioffer, Joffer;
        Ioffer = assignment[i];
        Joffer = assignment[j];
        double Ifree = freespace[Ioffer] +packages.get(i).getPeso();
        double Jfree = freespace[Joffer] +packages.get(j).getPeso();
        if(Ifree < packages.get(j).getPeso()|| Jfree < packages.get(i).getPeso()) return false;

        assignment[i] = Joffer;
        assignment[j] = Ioffer;
        freespace[Ioffer] = freespace[Ioffer] +packages.get(i).getPeso()-packages.get(j).getPeso();
        freespace[Joffer] = freespace[Joffer] +packages.get(j).getPeso()-packages.get(i).getPeso();
        return true;
    }

    /*!\Brief Mueve un paquete i a una oferta j en caso de que no se pueda retorna false
     *
     * \pre los valores han de ser validos
     */
    public boolean movePackage_p(int i, int j){
        if(freespace[j] < packages.get(i).getPeso()) return false;
        int OldOffert = assignment[i];
        assignment[i] = j;
        freespace[OldOffert] = freespace[OldOffert] +packages.get(i).getPeso();
        freespace[j] = freespace[j] -packages.get(i).getPeso();
        return true;
    }

    public String toString() {
        return "";
    }
}

class PaqueteComparatorPeso implements Comparator<Paquete>{

    public int compare(Paquete p1, Paquete p2){

        if( p1.getPeso() > p2.getPeso() ){
            return -1;
        }else if( p1.getPeso() < p2.getPeso() ){
            return 1;
        }else{
            return 0;
        }

    }
}

class PaqueteComparatorPrioridad implements Comparator<Paquete> {
    public int compare(Paquete p1, Paquete p2) {

        if (p1.getPrioridad() < p2.getPrioridad()) {
            return -1;
        } else if (p1.getPrioridad() > p2.getPrioridad()) {
            return 1;
        } else {
            return 0;
        }
    }
}

class OfertaComparator implements Comparator<Oferta>{

    public int compare(Oferta o1, Oferta o2){
        double oratio1 = o1.getPrecio()/o1.getPesomax();
        double oratio2 = o2.getPrecio()/o2.getPesomax();
        if( oratio1 > oratio2 ){
            return 1;
        }else if( oratio1 < oratio2 ){
            return -1;
        }else{
            return 0;
        }

    }
}
