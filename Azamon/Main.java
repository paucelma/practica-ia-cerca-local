import IA.Azamon.Paquete;
import IA.Azamon.Paquetes;
import IA.Azamon.Transporte;
import IA.Azamon.Oferta;
import IA.Azamon.AzamonBoard;
import IA.Azamon.AzamonSuccessorFunction;
import IA.Azamon.AzamonGoalTest;
import IA.Azamon.AzamonHeuristicFunction;
import IA.Azamon.AzamonHeuristicFunctionHappiness;
import IA.Azamon.AzamonSuccessorFunctionSA;

import aima.search.framework.Problem;
import aima.search.framework.Search;
import aima.search.framework.SearchAgent;
import aima.search.informed.HillClimbingSearch;
import aima.search.informed.SimulatedAnnealingSearch;

import java.sql.Time;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;  // Import the Scanner class

public class Main {

    public static void main(String[] args) throws Exception{

        /*
        TODO un print con las opciones del programa: -help, -end, -begin
        */
        int npackages = 100;
        float weight_ratio = 1.2f;
        int seed = 1234;
        int estadoIni = 1;
        double constHappy = 5;
        int heuristic = 0;
        for (int i = 0; i < args.length; i++) {
          switch (args[i]) {
          case "-p":
          try {
               npackages = Integer.parseInt(args[++i]);
              }
          catch (NumberFormatException e)
              {
               System.out.println(args[i] + " is not an integer.");
               System.exit(0);
              }

          break;

          case "-o":
          try {
               weight_ratio = Float.parseFloat(args[++i]);
              }
          catch (NumberFormatException e)
              {
                System.out.println(args[i] + " is not a float.");
                System.exit(0);
              }

          break;

          case "-s":
          try {
               seed = Integer.parseInt(args[++i]);
              }
          catch (NumberFormatException e)
              {
                System.out.println(args[i] + " is not an integer.");
                System.exit(0);
              }
          break;

          case "-i":
          try {
              estadoIni =  Integer.parseInt(args[++i]);
              }
          catch (NumberFormatException e)
              {
                System.out.println(args[i] + " is not an integer.");
                System.exit(0);
              }
          break;

          case "-h":
              try {
                  heuristic = Integer.parseInt(args[++i]);
              } catch (NumberFormatException e) {
                  System.out.println(args[i] + "is not an integer.");
                  System.exit(0);
              }
          break;

          default:
              // arg
              System.out.println("The option " + args[i] + " not valid.");
              System.out.println("-p [Number of packages] ");
            	System.out.println("-o [Weight ratio]");
              System.out.println("-s [Seed]");
            	System.out.println("-i [Initial state algorithm (0/1)]");
            	System.out.println("-h [Heuristic Function (0/1)]");
              System.exit(0);
              break;
          }
        }
        Paquetes paq = new Paquetes(npackages, seed);
        Transporte offer = new Transporte(paq, weight_ratio, seed);
        AzamonBoard board = new AzamonBoard(paq, offer, estadoIni, constHappy);
        if(heuristic == 0) {
            AzamonHillClimbingSearch(board);
            AzamonSimulatedAnnealingSearch(board);
        } else if(heuristic == 1) {
            AzamonHillClimbingSearchHappiness(board);
            AzamonSimulatedAnnealingSearchHappiness(board);
        }

     }


     private static void AzamonHillClimbingSearch(AzamonBoard board) {
        System.out.println("\nAzamon HillClimbing  -->");
        try {
            long time = System.currentTimeMillis();
            Problem problem =  new Problem(board,
            				new AzamonSuccessorFunction(),
            				new AzamonGoalTest(),
            				new AzamonHeuristicFunction());
            Search search =  new HillClimbingSearch();
            SearchAgent agent = new SearchAgent(problem,search);

            AzamonBoard newBoard = (AzamonBoard)search.getGoalState();
            time = System.currentTimeMillis() - time;
            System.out.println(newBoard.getPrice());
            printInstrumentation(agent.getInstrumentation());
            System.out.println(time + " ms");
            System.out.println();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void AzamonHillClimbingSearchHappiness(AzamonBoard board) {
       System.out.println("\nAzamon HillClimbingHappiness  -->");
       try {
           long time = System.currentTimeMillis();
           Problem problem =  new Problem(board,
                   new AzamonSuccessorFunction(),
                   new AzamonGoalTest(),
                   new AzamonHeuristicFunctionHappiness());
           Search search =  new HillClimbingSearch();
           SearchAgent agent = new SearchAgent(problem,search);

           AzamonBoard newBoard = (AzamonBoard)search.getGoalState();
           time = System.currentTimeMillis() - time;
           System.out.println(newBoard.getPriceHappiness());
           printInstrumentation(agent.getInstrumentation());
           System.out.println(time + " ms");
           System.out.println();
       } catch (Exception e) {
           e.printStackTrace();
       }
   }

    private static void AzamonSimulatedAnnealingSearch(AzamonBoard board) {
        System.out.println("\nAzamon Simulated Annealing  -->");
        try {
            long time = System.currentTimeMillis();
            Problem problem =  new Problem(board,
            				new AzamonSuccessorFunctionSA(),
            				new AzamonGoalTest(),
            				new AzamonHeuristicFunction());
            SimulatedAnnealingSearch search =  new SimulatedAnnealingSearch(25000,100,25,0.01);
            //search.traceOn();
            SearchAgent agent = new SearchAgent(problem,search);


            AzamonBoard newBoard = (AzamonBoard)search.getGoalState();
            time = System.currentTimeMillis() - time;
            System.out.println(newBoard.getPrice());
            System.out.println(time + " ms");
            printInstrumentation(agent.getInstrumentation());
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void AzamonSimulatedAnnealingSearchHappiness(AzamonBoard board) {
        System.out.println("\nAzamon Simulated Annealing Happiness -->");
        try {
            long time = System.currentTimeMillis();
            Problem problem =  new Problem(board,
                    new AzamonSuccessorFunctionSA(),
                    new AzamonGoalTest(),
                    new AzamonHeuristicFunctionHappiness());
            SimulatedAnnealingSearch search =  new SimulatedAnnealingSearch(25000,100,25,0.01);
            //search.traceOn();
            SearchAgent agent = new SearchAgent(problem,search);

            AzamonBoard newBoard = (AzamonBoard)search.getGoalState();
            time = System.currentTimeMillis() - time;
            System.out.println(newBoard.getPriceHappiness());
            System.out.println(time + " ms");
            printInstrumentation(agent.getInstrumentation());
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void printInstrumentation(Properties properties) {
        Iterator keys = properties.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            String property = properties.getProperty(key);
            System.out.println(key + " : " + property);
        }

    }
    //TODO quiza esto no nos interesa

    private static void printActions(List actions) {
        for (int i = 0; i < actions.size(); i++) {
            String action = (String) actions.get(i);
            System.out.println(action);
        }
    }

    //enseñar el problema inicial
    private static void showProblem(Paquetes pak, Transporte offer)
    {
    	System.out.println("Packages");
    	System.out.println("ID \tWeight \t\tPriority");
    	System.out.println("--------------------------------");
    	for(int i=0; i<pak.size(); i++){
    		Paquete pac = pak.get(i);
    		System.out.printf("%d \t",i);
    		System.out.printf("%f \t",pac.getPeso());
    		System.out.printf("%d \n",pac.getPrioridad());
    	}
    	System.out.println("Offerts");
    	System.out.println("ID \tMaxWeight \tPrice \t\tDays");
    	System.out.println("--------------------------------------------");
    	for(int i=0; i<offer.size(); i++){
    		Oferta oferta = offer.get(i);
    		System.out.printf("%d \t",i);
    		System.out.printf("%f \t",oferta.getPesomax());
    		System.out.printf("%f \t",oferta.getPrecio());
    		System.out.printf("%d \n",oferta.getDias());
    	}
    }
}
