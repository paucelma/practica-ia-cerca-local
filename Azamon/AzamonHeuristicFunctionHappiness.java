package IA.Azamon;

import aima.search.framework.HeuristicFunction;

public class AzamonHeuristicFunctionHappiness implements HeuristicFunction  {
/*
  public boolean equals(Object obj) {
      boolean retValue;

      retValue = super.equals(obj);
      return retValue;
  }
  */
  public double getHeuristicValue(Object state) {
      AzamonBoard board=(AzamonBoard)state;
    /*  int np;
      double costPackOff,costStorage,sum=0;
      np=board.getNPackages();
      int [] assignment = board.getAssignment();
      double pesoPack;
      for(int i=0;i<np;i++){
          pesoPack = board.getPeso(i);
          costPackOff = board.getOfferPrice(assignment[i])*pesoPack;
          costStorage = board.getDayOffer(assignment[i])*0.25*pesoPack;
          sum = sum + costPackOff + costStorage - board.getPriorityPrice(i) - board.getDiasAnte(i, assignment[i])*5;
      }
      return (sum);
      */
      return board.getPriceHappiness();
  }
}
